from django.shortcuts import render, redirect
from .models import Reviews
from .forms import ReviewsForm
from django.views.generic import DetailView


class ReviewsDetailView(DetailView):
    model = Reviews
    template_name = 'reviews/details.html'
    context_object_name = 'article'


def reviews_home(request):
    reviews = Reviews.objects.order_by('-date')
    return render(request, 'reviews/reviews_home.html', {'reviews': reviews})


def create(request):
    error = ''
    if request.method == 'POST':
        form = ReviewsForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('reviews')
        else:
            error = 'Форма была неверной'

    form = ReviewsForm

    data = {
        'form': form,
        'error': error
    }
    return render(request, 'reviews/create.html', data)
