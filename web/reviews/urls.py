from django.urls import path
from . import views

urlpatterns = [
    path('', views.reviews_home, name='reviews'),
    path('create', views.create, name='create_review'),
    path('<int:pk>', views.ReviewsDetailView.as_view(), name='reviews_detail')
]
