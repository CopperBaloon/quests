from django.db import models


class Reviews(models.Model):
    title = models.CharField('Краткая характеристика', max_length=50, default='')
    rate = models.IntegerField('Оценка от 1 до 10')
    full_text = models.TextField('Отзыв')
    date = models.DateTimeField('Дата публикации')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
