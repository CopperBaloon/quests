from .models import Reviews
from django.forms import ModelForm, TextInput, DateInput, Textarea, NumberInput


class ReviewsForm(ModelForm):
    class Meta:
        model = Reviews
        fields = ['title', 'rate', 'full_text', 'date']

        widgets ={
            "title": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Краткое описание'
            }),
            "rate": NumberInput(attrs={
                'class': 'form-control',
                'placeholder': 'Оценка от 1 до 10'
            }),
            "date": DateInput(attrs={
                'class': 'form-control',
                'placeholder': 'Дата посещения'
            }),
            "full_text": Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Ваш отзыв'
            }),

        }
