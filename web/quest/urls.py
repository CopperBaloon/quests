from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.quest_home, name='quest'),
    path('schedule/', include('timetable.urls'))
]