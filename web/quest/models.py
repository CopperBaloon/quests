from django.db import models


class Quest(models.Model):
    title = models.CharField('Название', max_length=50, default='')
    rate = models.IntegerField('Рейтинг сложности')
    full_text = models.TextField('Описание')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Квест'
        verbose_name_plural = 'Квесты'






