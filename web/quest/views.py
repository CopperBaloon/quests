from django.shortcuts import render, redirect
from .models import Quest
from django.views.generic import DetailView



def quest_home(request):
    quest = Quest.objects.all()
    return render(request, 'quest/quest_home.html', {'quest': quest})


