from django.shortcuts import render
from .models import Articles
from django.views.generic import DetailView


def main_page(request):
    news = Articles.objects.order_by('-date')
    data = {
        'title': 'Главная страница',
        'news': news
    }
    return render(request, 'main/main_page.html', data)


class NewsDetailView(DetailView):
    model = Articles
    template_name = 'main/details.html'
    context_object_name = 'article'


def about(request):
    return render(request, 'main/about.html')
