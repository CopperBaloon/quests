from django.db import models


class Schedule(models.Model):
    id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    title = models.CharField('Название', max_length=50)
    time = models.TimeField('Время начала')
    sum = models.IntegerField('Стоимость')
    reservation = models.BooleanField('Свободно')
    name = models.CharField('Ваше имя', max_length=50, default='')
    phone = models.CharField('Телефон', max_length=10, default='')

    class Meta:
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписание'

    def __str__(self):
        return self.title
