from .models import Schedule
from django.forms import ModelForm, TextInput, DateInput, Textarea, NumberInput


class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields = ['title', 'time', 'sum', 'reservation', 'name', 'phone']
        widgets = {
            "name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Ваше имя'
            }),
            "phone": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Ваш телефон'
            })
        }
