from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm
from django.views.generic import UpdateView


def timetable(request):
    quest = Schedule.objects.all()
    return render(request, 'timetable/timetable.html', {'quest': quest})


def book(request):
    error = ''
    quest = Schedule
    if request.method == 'POST':
        form = ScheduleForm(request.POST)

        return redirect('timetable')

    form = ScheduleForm

    data = {
        'quest': quest,
        'form': form,
        'error': error
    }
    return render(request, 'timetable/book.html', data)
